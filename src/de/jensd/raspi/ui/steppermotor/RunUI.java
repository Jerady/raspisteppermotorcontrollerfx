/*
 * Copyright (c) 2013, Jens Deters
 *
 * All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package de.jensd.raspi.ui.steppermotor;

import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/*
 * @author Jens Deters
 *
 */
public class RunUI extends Application {

    private final static Logger LOGGER = Logger.getLogger(RunUI.class.getName());

    @Override
    public void init() throws Exception {
        Font.loadFont(getClass().getResource("OCRASTD.TTF").toExternalForm(), 12);
        Locale.setDefault(Locale.ENGLISH);
        LOGGER.setLevel(Level.INFO);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        Parent root = new Stepper();
        Scene scene = new Scene(root, 1280, 720);
        String css = getClass().getResource("stepper.css").toExternalForm();
        scene.getStylesheets().addAll(css);
        primaryStage.setScene(scene);
        primaryStage.setTitle("StepperMotor Control");
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);

    }
}
